// Import the functions you need from the SDKs you need
import { initializeApp }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase, onValue, ref, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA0Ax7VK8FVUhxmN3taV_p4s8lf_pSZlEA",
    authDomain: "uwuweb-a5715.firebaseapp.com",
    databaseURL: "https://uwuweb-a5715-default-rtdb.firebaseio.com",
    projectId: "uwuweb-a5715",
    storageBucket: "uwuweb-a5715.appspot.com",
    messagingSenderId: "594016288438",
    appId: "1:594016288438:web:90b9c299904a875c75b017",
    measurementId: "G-WR18HMG23H"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var precio = "";
var nombre = "";
var carrera = "";
var genero = "";
var url = "";
var archivos = "";

// Botones

var btnAgregar = document.getElementById('btnAgregar');
var btnMostrar = document.getElementById('btnMostrar');
var btnModificar = document.getElementById('btnModificar');
var btnBorrar = document.getElementById('btnBorrar');
var btnLimpiar = document.getElementById('btnLimpiar');
var btnTodos = document.getElementById("btnTodos");
var lista = document.getElementById("lista");
var archivo = document.getElementById("archivo");
var verImagen = document.getElementById("verImagen");

// funciones

function insertarDatos() {
    leerImputs();
    set(ref(db, 'productos/' + nombre), {
        precio: precio,
        carrera: carrera,
        genero: genero,
    }).then((resp) => {
        alert("Se realizó el registro correctamente :D");
        mostrarProductos();
    }).catch((error) => {
        alert("Surgió un error: " + error);
    })
}

function mostrarDatos() {
    leerImputs();
    const dbref = ref(db);
    get
        (child(dbref, 'productos/' + nombre))
        .then((snapshot) => {
            if (snapshot.exists()) {
                precio = snapshot.val().precio;
                carrera = snapshot.val().carrera;
                genero = snapshot.val().genero;
                archivo = snapshot.val().archivo;
                url = snapshot.val().url;
                llenarImputs();
            }
            else {
                alert("No existe el registro solicitado :c");
            }
        }).catch((error) => {
            alert("Surgió un error: " + error);
        })
}

function mostrarProductos() {
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
        lista.innerHTML = ""
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            lista.innerHTML = "<div class='campo'> " + lista.innerHTML + "Nombre: " +  childKey + " | Precio: " + "$" +
                childData.precio + " | Carrera: " + childData.carrera + " | Genero: " + childData.genero + "<br> </div>";
            console.log(childKey + ":");
            console.log(childData.nombre)
            // ...
        });
    }, {
        onlyOnce: true
    });
}

function llenarImputs() {
    document.getElementById('nombre').value = nombre;
    document.getElementById('carrera').value = carrera;
    document.getElementById('genero').value = genero;
    document.getElementById('imgNombre').value = archivo;
    document.getElementById('url').value = url;
}

function modificarDatos() {
    leerImputs();
    update(ref(db, 'productos/' + nombre), {
        precio: precio,
        carrera: carrera,
        genero: genero
    }).then(() => {
        alert("Se realizó la actualización");
        mostrarProductos();
    }).catch(() => {
        alert("Ocurrió un error :C " + error);
    });
}

function borrarDatos() {
    leerImputs();
    remove(ref(db, 'productos/' + nombre)).then(() => {
        alert("Se eliminó el registro");
        mostrarProductos();
        limpiar();
    }).catch(() => {
        alert("Ocurrió un error " + error);
    })
}

function leerImputs() {
    precio = document.getElementById('precio').value;
    nombre = document.getElementById('nombre').value;
    carrera = document.getElementById('carrera').value;
    genero = document.getElementById('genero').value;
    archivos = document.getElementById('imgNombre').value;
    url = document.getElementById('url').value;
}

function limpiar() {
    document.getElementById('precio').value = "";
    document.getElementById('nombre').value = "";
    document.getElementById('carrera').value = "";
    document.getElementById('genero').value = "0";
    document.getElementById('imgNombre').value = "";
    document.getElementById('archivo').value = "";
    document.getElementById('verImagen').value = "";
}

async function CargarImagen() {
    const file = event.target.files[0]
    const name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);
    uploadBytes(storageRef, file).then((snapshot) => {
        descargarImagen(name);
        alert("Se cargó la imagen")
    }).catch((error) => {
        console.log(error);
    });
}

async function descargarImagen() {
    archivo = document.getElementById('imgNombre').value;
    alert("El archivo es:" + 'imagenes/' + archivo)
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/' + archivo);

    // Get the download URL
    getDownloadURL(starsRef)
        .then((url) => {
            // Insert url into an <img> tag to "download"
            // document.getElementById('imagen').src=url;
            console.log(url);

            document.getElementById('imagen').src = url;
            document.getElementById('url').value = url;

        })
        .catch((error) => {
            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            switch (error.code) {
                case 'storage/object-not-found':
                    console.log("No se encontro la imagen")
                    break;
                case 'storage/unauthorized':
                    console.log("NO Tiene permisos para accesar imagen")
                    break;
                case 'storage/canceled':
                    console.log("Se canceló la subida");
                    break;
                // ...
                case 'storage/unknown':
                    // Unknown error occurred, inspect the server response
                    break;
            }
        });
}

// codificacion de eventos click de los botones

btnAgregar.addEventListener('click', insertarDatos);
btnMostrar.addEventListener('click', mostrarDatos);
btnModificar.addEventListener('click', modificarDatos);
btnBorrar.addEventListener('click', borrarDatos);
btnTodos.addEventListener('click', mostrarProductos);
btnLimpiar.addEventListener('click', limpiar);
archivo = addEventListener('change', CargarImagen);
verImagen.addEventListener('click', descargarImagen);




