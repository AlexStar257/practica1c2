// Import the functions you need from the SDKs you need
import { initializeApp }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase, onValue, ref, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA0Ax7VK8FVUhxmN3taV_p4s8lf_pSZlEA",
    authDomain: "uwuweb-a5715.firebaseapp.com",
    databaseURL: "https://uwuweb-a5715-default-rtdb.firebaseio.com",
    projectId: "uwuweb-a5715",
    storageBucket: "uwuweb-a5715.appspot.com",
    messagingSenderId: "594016288438",
    appId: "1:594016288438:web:90b9c299904a875c75b017",
    measurementId: "G-WR18HMG23H"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

var login = document.getElementById("ingresar");
var usuario = "";
var contrasena = "";
login.addEventListener('click', comprobarUsuario);

function extraerInfo() {
    usuario = document.getElementById("usuario").value;
    contrasena = document.getElementById("contrasena").value;
}

function comprobarUsuario() {
    extraerInfo();
    const dbref = ref(db);
    get(child(dbref, 'usuarios/' + usuario))
    .then((snapshot) => {
        if (snapshot.exists()) {
            if (contrasena == snapshot.val().contraseña) {
                alert("Bienvenido " + usuario)
                window.open("/html/admin.html")
            }
            else {
                alert("Contraseña incorrecta, vuelva a intentarlo")
                window.open("/html/error.html")
            }
        }
        else {
            alert("No existe el usuario");
        }
    }).catch((error) => {
        alert("error comprobarUsuario" + error);
    });
}
