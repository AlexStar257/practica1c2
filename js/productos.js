// Import the functions you need from the SDKs you need
import { initializeApp }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import { getDatabase, onValue, ref, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA0Ax7VK8FVUhxmN3taV_p4s8lf_pSZlEA",
    authDomain: "uwuweb-a5715.firebaseapp.com",
    databaseURL: "https://uwuweb-a5715-default-rtdb.firebaseio.com",
    projectId: "uwuweb-a5715",
    storageBucket: "uwuweb-a5715.appspot.com",
    messagingSenderId: "594016288438",
    appId: "1:594016288438:web:90b9c299904a875c75b017",
    measurementId: "G-WR18HMG23H"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

var producto = document.getElementById('productos');

function extraerProductos() {
    const db = getDatabase();
    const dbRef = ref(db, 'productos/');
    onValue(dbRef, (snapshot) => {
        producto.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if (childData.estado == "0") {
                producto.innerHTML = producto.innerHTML + "<tr>" + "<td>" +
                    "<p>" + childData.nombre + "</p>" +
                    "<img src='" + childData.url + "' alt=''>" +
                    "<p>" + "$" + childData.precio + "<br>" + "Descripción: " + childData.descripcion + "</p>" +
                    "</td>" + "</tr>";
            }
        });
        {
            onlyOnce: true
        }
    });
}
window.onload(extraerProductos());
